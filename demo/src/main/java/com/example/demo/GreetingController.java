package com.example.demo;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class GreetingController {
	
	@GetMapping("/add")
	public String sayHello(@RequestParam int a, @RequestParam int b, Model model) {
		model.addAttribute("param_a", a);
		model.addAttribute("param_b", b);
		model.addAttribute("soma", a+b);
		return "addpage";
		
	}

	/*
	@GetMapping("/greeting")
	public String greeting(@RequestParam(name="name", required=false, defaultValue="World") String name, Model model) {
		model.addAttribute("name", name);
		return "greeting";
	}
	*/

}